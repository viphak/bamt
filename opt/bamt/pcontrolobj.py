#!/usr/bin/python

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.
#
#    pcontrol borrows/copies heavily from atitweak by Mark / mjmvisser 
#    https://bitcointalk.org/index.php?topic=25750.0
#    
#    If you like pcontrol, please show some love to atitweak:
#    1Kh3DsAhiu65EC7DFFHDGoGowAp5usQrCG
#


class pcontrolobj( object ):

	inmenu = 0
	drawnow = 0
	
	displaytype = 0
	numDisplays = 4
	outQueues = [ ]
	gpuData = [ ]
	currentGPU = 0	
	numGPUs = 0
	escape = 0
	adl_time = 3
	config = { } 
	
	def __init__(self, confdict):
		self.config = confdict
	
	def getGPUData(self, gpu):
		return self.gpuData[gpu]
		
	def setGPUData(self, gpu, data):
		self.gpuData[gpu] = data	
	
	def getInMenu(self):	
		return self.inmenu
		
	def getDrawNow(self):	
		if self.drawnow == True:
			self.drawnow = False
			return True
			                    
		return False

	def hasGPUSetting(self, gpu, setting):	
		if setting in self.config['gpu' + str(gpu)].keys():
			return True
		return False
		
	def getGPUSetting(self, gpu, setting):	
		if setting in self.config['gpu' + str(gpu)].keys():
			return self.config['gpu' + str(gpu)][setting]
		return None	
		
	def hasSetting(self, setting):	
		if setting in self.config['settings'].keys():
			return True
		return False
		
	def getSetting(self, setting):	
		if setting in self.config['settings'].keys():
			return self.config['settings'][setting]
		return None		
		
	def getTargetTempMin(self, gpu):
		if 'autofan_target_min' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_target_min']))
		elif 'autofan_target_max' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_target_max']) - 2)
		elif 'autofan_target_min' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_target_min']))
		elif 'autofan_target_max' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_target_max']) - 2)			
		else:
			return 0                 
	
	def setTargetTempMin(self, gpu, temp):
		if gpu < 0:
			self.config['settings']['autofan_target_min'] = temp
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_target_min'] = temp


	def getTargetTempMax(self, gpu):
		if 'autofan_target_max' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_target_max']))
		elif 'autofan_target_max' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_target_max']))
		else:
			return 0
	
	def setTargetTempMax(self, gpu, temp):
		if gpu < 0:
			self.config['settings']['autofan_target_max'] = temp
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_target_max'] = temp
			


	def getOverheatTemp(self, gpu):
		if 'autofan_overheat' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_overheat']))
		elif 'autofan_overheat' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_overheat']))
		else:
			return 85
			
	def setOverheatTemp(self, gpu, temp):
		if gpu < 0:
			self.config['settings']['autofan_overheat'] = temp
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_overheat'] = temp
		
			
	def getCutoffTemp(self, gpu):
		if 'autofan_cutoff' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_cutoff']))
		elif 'autofan_cutoff' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_cutoff']))
		else:
			return 85
			
	def setCutoffTemp(self, gpu, temp):
		if gpu < 0:
			self.config['settings']['autofan_cutoff'] = temp
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_cutoff'] = temp
	
	def getFanMaxSpeed(self, gpu):
		if 'autofan_max' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_max']))
		elif 'autofan_max' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_max']))
		else:
			return 100
	
	def setFanMaxSpeed(self, gpu, per):
		if gpu < 0:
			self.config['settings']['autofan_max'] = per
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_max'] = per
	
	def getFanMinSpeed(self, gpu):
		if 'autofan_min' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_min']))
		elif 'autofan_min' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_min']))
		else:
			return 10
	
	def setFanMinSpeed(self, gpu, per):
		if gpu < 0:
			self.config['settings']['autofan_min'] = per
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_min'] = per
	
	
	
	def getAutoCoreLo(self, gpu):
		if 'autocore_speed_lo' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autocore_speed_lo']))
		else:
			return -1
			
	def setAutoCoreLo(self, gpu, rate):
		if gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autocore_speed_lo'] = rate
	
			
	def getAutoCoreHi(self, gpu):
		if 'autocore_speed_hi' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autocore_speed_hi']))
		elif 'core_speed_2' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['core_speed_2']))
		else:
			return -1
			
	def setAutoCoreHi(self, gpu, rate):
		if gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autocore_speed_hi'] = rate
	
	
			
	def getAutoCoreStep(self, gpu):
		if 'autocore_speed_step' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autocore_speed_step']))
		else:
			return 20
			
	def setAutoCoreStep(self, gpu, rate):
		if gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autocore_speed_step'] = rate
	
	
	def getTempPeriodDown(self, gpu):
		if 'autofan_period_down' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_period_down']))
		elif 'autofan_period_down' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_period_down']))
		else:
			return 60
	
	def setTempPeriodDown(self, gpu, per):
		if gpu < 0:
			self.config['settings']['autofan_period_down'] = per
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_period_down'] = per
	
		
	def getTempPeriodUp(self, gpu):
		if 'autofan_period_up' in self.config['gpu' + str(gpu)].keys():
			return(int(self.config['gpu' + str(gpu)]['autofan_period_up']))
		elif 'autofan_period_up' in self.config['settings'].keys():
			return(int(self.config['settings']['autofan_period_up']))
		else:
			return 5
		
	def setTempPeriodUp(self, gpu, per):
		if gpu < 0:
			self.config['settings']['autofan_period_up'] = per
		elif gpu < self.numGPUs:
			self.config['gpu' + str(gpu)]['autofan_period_up'] = per	
		
	           
	
		
	def getDisplayType(self):
		return self.displaytype
	
	def getOutQueue( self, gpu ):
		return self.outQueues[gpu]
		
	def sendToAll( self, msg ):
		for q in self.outQueues:
			q.put(msg,True)

	def getCurrentGPU(self):
		return self.currentGPU
		
	def getNumGPUs(self):
		return self.numGPUs
	
	def setNumGPUs(self, gpus):
		self.numGPUs = gpus
		while (len(self.gpuData) <= gpus):
			self.gpuData.append( { } )
		
			
	def getADL_Time(self):
		return self.adl_time

