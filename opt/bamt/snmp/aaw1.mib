
BAMT1 DEFINITIONS ::= BEGIN

IMPORTS
	OBJECT-TYPE, MODULE-IDENTITY, enterprises, 
	Integer32
		FROM SNMPv2-SMI
	DisplayString
		FROM SNMPv2-TC;

-- first shot at creating a mib..

aaronwolfe-mib MODULE-IDENTITY
	LAST-UPDATED "201203010425Z"
	ORGANIZATION 
		"Aaron Wolfe"
	CONTACT-INFO 
		"aaron@aaronwolfe.com"
	DESCRIPTION 
		"currently only used for BAMT"
::= { aaronwolfe 1 }

aaronwolfe           OBJECT IDENTIFIER ::= { enterprises 5454 }
bamt                 OBJECT IDENTIFIER ::= { aaronwolfe 2 }
settings             OBJECT IDENTIFIER ::= { bamt 1 }

minerid  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { settings 1 }

minerloc  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { settings 2 }

gpuTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF GpuEntry
	MAX-ACCESS not-accessible
	STATUS     current
	DESCRIPTION 
		""
	::= { bamt 2 }

gpuEntry  OBJECT-TYPE
	SYNTAX 	GpuEntry
	MAX-ACCESS not-accessible
	STATUS     current
	DESCRIPTION 
		""
	INDEX { gpuNumber }
	::= { gpuTable 1 }

GpuEntry ::= SEQUENCE {
	gpuNumber
		Integer32,
	gpuCLID
		DisplayString,
	gpuPCIID
		DisplayString,
	gpuXID
		DisplayString,	
	device
		DisplayString,
	vendor
		DisplayString,
	sdevice
		DisplayString,
	svendor
		DisplayString,
	family
		DisplayString,
	cores
		Integer32,
	enabled
		Integer32
}

gpuNumber  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 1 }

gpuCLID  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 2 }

gpuPCIID  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 3 }

gpuXID  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 4 }

device  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 5 }

vendor  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 6 }

sdevice  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 7 }

svendor  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 8 }

family  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 9 }

cores  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 10 }

enabled  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuEntry 11 }



gpuStatusTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF GpuStatusEntry
	MAX-ACCESS not-accessible
	STATUS     current
	DESCRIPTION 
		""
	::= { bamt 3 }

gpuStatusEntry  OBJECT-TYPE
	SYNTAX 	GpuStatusEntry
	MAX-ACCESS not-accessible
	STATUS     current
	DESCRIPTION 
		""
	INDEX { gpuNumber }
	::= { gpuStatusTable 1 }

GpuStatusEntry ::= SEQUENCE {
	engineClock
		Integer32,
	memoryClock
		Integer32,
	coreVoltage
		Integer32,
	performanceLevel
		Integer32,
	load
		Integer32,
	fanPercent
		Integer32,
	fanRPM
		Integer32,
	temperature
		Integer32,
	powerTune
		Integer32,		
	errors
		Integer32,	
}

engineClock  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 1 }

memoryClock  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 2 }

coreVoltage  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		"core voltage * 1000"
	::= { gpuStatusEntry 3 }

performanceLevel  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 4 }

load  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 5 }

fanPercent  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 6 }

fanRPM  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 7 }

temperature  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 8 }

powerTune  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 9 }

errors  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuStatusEntry 10 }


gpuMiningTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF GpuMiningEntry
	MAX-ACCESS not-accessible
	STATUS     current
	DESCRIPTION 
		""
	::= { bamt 4 }

gpuMiningEntry  OBJECT-TYPE
	SYNTAX 	GpuMiningEntry
	MAX-ACCESS not-accessible
	STATUS     current
	DESCRIPTION 
		""
	INDEX { gpuNumber }
	::= { gpuMiningTable 1 }

GpuMiningEntry ::= SEQUENCE {
	status
		DisplayString,
	miner
		DisplayString,
	kernel
		DisplayString,
	kernelOptions
		DisplayString,
	sharesAccepted
		Integer32,
	sharesRejected
		Integer32,
	sharesStale
		Integer32,
	sharesOther
		Integer32,
	rateCurrent
		Integer32,
	rateAverage
		Integer32,
	uptime
		Integer32,
	poolURL
		DisplayString,
	lastShareTime
		Integer32,
}

status  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 1 }

miner  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 2 }

kernel  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 3 }

kernelOptions  OBJECT-TYPE
	SYNTAX     DisplayString
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 4 }

sharesAccepted  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 5 }

sharesRejected  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 6 }

sharesStale OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 7 }

sharesOther  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 8 }

rateCurrent  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 9 }

rateAverage  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 10 }

uptime  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 11 }

poolURL OBJECT-TYPE
        SYNTAX     DisplayString
        MAX-ACCESS read-only
        STATUS     current
        DESCRIPTION
                ""
        ::= { gpuMiningEntry 12 }

lastShareTime  OBJECT-TYPE
	SYNTAX     Integer32
	MAX-ACCESS read-only
	STATUS     current
	DESCRIPTION 
		""
	::= { gpuMiningEntry 13 }
    
END


