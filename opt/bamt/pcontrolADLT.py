#!/usr/bin/python

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.
#
#    pcontrol borrows/copies heavily from atitweak by Mark / mjmvisser 
#    https://bitcointalk.org/index.php?topic=25750.0
#    
#    If you like pcontrol, please show some love to atitweak:
#    1Kh3DsAhiu65EC7DFFHDGoGowAp5usQrCG
#

import os, sys, threading

from adl3 import *
import collections
import curses
import time
import datetime


class ADLError(Exception):
    pass

class pcontrolADLT( threading.Thread ):

	adapters = []

	def __init__( self, pcontrol, stdscr, evtlog ):
		self.pcontrol = pcontrol
		self.scr = stdscr	
		self.evtlog = evtlog
		
		threading.Thread.__init__ ( self )
		self.setDaemon ( True )
		self.dat = { }

		self.initialize()


	def getts( self ):
		return datetime.datetime.now().strftime("%I:%M:%S")


	def getd( self, key ):
		if (key in self.dat.keys()):
			return self.dat[key]
		else:
			return None
		
		
	def setd( self, key, val, stime=None ):
		self.dat[key] = val
		
		if (stime is None):
			self.dat[key + "_time"] = time.time()
		else:
			self.dat[key + "_time"] = stime
		
		
	def hasd(self, key):
		if (key in self.dat.keys()):
			return True
		else:
			return False
			
	def incd(self, key):
		if (key in self.dat.keys()):
			self.dat[key] = self.dat[key] + 1
		else:
			self.dat[key] = 1

	def checkGPUOverMax(self, temps, index):
		samps = int( self.pcontrol.getTempPeriodUp(index)  / self.pcontrol.getADL_Time() )
		
		if (len(temps[index]) < samps) or (self.pcontrol.getTargetTempMax(index) == 0):
			# not enough samples/not managing fan
			return False
		
		if (sum( temps[index][:samps] ) / samps) > self.pcontrol.getTargetTempMax(index):
			return True
			
		return False
		
		

	def checkGPUUnderMin(self, temps, index):
		samps = int( self.pcontrol.getTempPeriodDown(index)  / self.pcontrol.getADL_Time() )
		
		if (len(temps[index]) < samps) or (self.pcontrol.getTargetTempMin(index) == 0):
			# not enough samples/not managing fan
			return False
		
		if (sum( temps[index][:samps] ) / samps) < self.pcontrol.getTargetTempMin(index):
			return True
			
		return False


	def set_fan( self, index, speed):
		
		fan_speed_value = ADLFanSpeedValue()
		fan_speed_value.iSize = sizeof(fan_speed_value)
		fan_speed_value.iSpeedType = ADL_DL_FANCTRL_SPEED_TYPE_PERCENT
		fan_speed_value.iFanSpeed = speed
		fan_speed_value.iFlags = ADL_DL_FANCTRL_FLAG_USER_DEFINED_SPEED
						
		if ADL_Overdrive5_FanSpeed_Set(index, 0, byref(fan_speed_value)) != ADL_OK:
			self.evtlog.append(self.getts() + ": ERR fan set failed")



	def set_plevels(self, adapter_list=None, plevel_list=None, engine_clock=None, memory_clock=None, core_voltage=None):
		adapter_info = self.get_adapter_info()
	
		for adapter_index, info in enumerate(adapter_info):
			if adapter_list is None or adapter_index in adapter_list:
				od_parameters = ADLODParameters()
				od_parameters.iSize = sizeof(od_parameters)
				
				if ADL_Overdrive5_ODParameters_Get(info.iAdapterIndex, byref(od_parameters)) != ADL_OK:
					self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : ADL_Overdrive5_ODParameters_Get failed.")
					
				if od_parameters.iDiscretePerformanceLevels:
					plevels = ADLODPerformanceLevels()
					plevels_size = sizeof(ADLODPerformanceLevels) + sizeof(ADLODPerformanceLevel) * (od_parameters.iNumberOfPerformanceLevels -1)
					resize(plevels, plevels_size)
					plevels.iSize = plevels_size
			
					if ADL_Overdrive5_ODPerformanceLevels_Get(info.iAdapterIndex, 0, byref(plevels)) != ADL_OK:
						self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : ADL_Overdrive5_ODPerformanceLevels_Get failed.")
			
					levels = cast(plevels.aLevels, POINTER(ADLODPerformanceLevel))
					
					for plevel_index in range(0, od_parameters.iNumberOfPerformanceLevels):
						if plevel_list is None or plevel_index in plevel_list:
							message = []
							
							if engine_clock is not None:
								levels[plevel_index].iEngineClock = int(engine_clock*100.0)
								self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : setting engine clock to " + str(engine_clock))
							if memory_clock is not None:
								levels[plevel_index].iMemoryClock = int(memory_clock*100.0)
								self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : setting memory clock to " + str(memory_clock))
							if core_voltage is not None:
								levels[plevel_index].iVddc = int(core_voltage*1000.0)
								self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : setting core voltage to " + str(core_voltage))
							
					# set the performance levels for this adapter            
					if ADL_Overdrive5_ODPerformanceLevels_Set(info.iAdapterIndex, byref(plevels)) != ADL_OK:
						self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : ADL_Overdrive5_ODPerformanceLevels_Set failed.")
					
				else:
					self.evtlog.append(self.getts() + ": GPU " + str(adapter_index) + " : Adapter does not support discrete performance levels.")


	def getTattr( self, index, temp):
		tattr = curses.A_BOLD
		
		if ( temp > self.pcontrol.getOverheatTemp(index) ):
			# overheating
			tattr = curses.color_pair(1) | curses.A_BOLD
		else:
			if ( self.pcontrol.getTargetTempMax(index) > 0) and ( temp > self.pcontrol.getTargetTempMax(index)):
				# too hot
				tattr = curses.color_pair(3) | curses.A_BOLD
			elif (temp == 0): 
				tattr = curses.color_pair(8)  | curses.A_BOLD
			elif ( self.pcontrol.getTargetTempMin(index) > 0) and ( temp < self.pcontrol.getTargetTempMin(index)):
				# too cold
				tattr = curses.color_pair(6) | curses.A_BOLD
			else:
				# in range
				tattr = curses.color_pair(2) | curses.A_BOLD
			
		return tattr


	def run ( self ):
	
		adapter_info = self.get_adapter_info()

		fans = [ ]
		overheat = [ ]
		temp_hist = [ ]
		fallback = [ ]
		temps = [ ]
		
		# setup some things	
		for index, info in enumerate(adapter_info):			
				
			while (len(temps) < index+1):
				temps.append( [ ] )
			
			while (len(fans) < index+1):
				fans.append(0)
				
			while (len(overheat) < index+1):
				overheat.append(0)
			
			while (len(fallback) < index+1):
				fallback.append(0)
			
			
			while (len(temp_hist) < index+1):
				temp_hist.append( { 'min': { }, 'max': { }, 'avg': { }, 'smp': { }, 'tot': { }, 'lst': { } } )
			
			for x in range(0,21):
				temp_hist[index]['min'][(x*5)] = 0  
				temp_hist[index]['max'][(x*5)] = 0  
				temp_hist[index]['avg'][(x*5)] = 0  
				temp_hist[index]['smp'][(x*5)] = 0 
				temp_hist[index]['tot'][(x*5)] = 0
				temp_hist[index]['lst'][(x*5)] = 0
		
	
		while(1):
		
			overheating = 0
			for o in overheat:
				if o > 0:
					overheating = 1
		
			for index, info in enumerate(adapter_info):

				self.dat = self.pcontrol.getGPUData(index)

				temperature = ADLTemperature()
				temperature.iSize = sizeof(temperature)
					
				if ADL_Overdrive5_Temperature_Get(info.iAdapterIndex, 0, byref(temperature)) != ADL_OK:
					self.evtlog.append(self.getts() + ": GPU " + str(index) + " : ADL_Overdrive5_Temperature_Get failed.")
					self.setd('temp', -1)
				else:
					self.setd('temp', temperature.iTemperature/1000.0)
					
					temps[index].insert(0, self.getd('temp'))
					
					# throw away extra
					while ( len(temps[index]) > max(self.pcontrol.getTempPeriodDown(index),self.pcontrol.getTempPeriodUp(index))):
							del temps[index][-1]
					
					if (self.pcontrol.getOverheatTemp(index) > 0):
						if (self.getd('temp') > self.pcontrol.getOverheatTemp(index)):
							overheat[index] = overheat[index] + 1
							self.incd('overheat')
						else:
							overheat[index] = 0
							

					
					
				activity = ADLPMActivity()
				activity.iSize = sizeof(activity)
				
				if ADL_Overdrive5_CurrentActivity_Get(info.iAdapterIndex, byref(activity)) != ADL_OK:
					self.evtlog.append(self.getts() + ": GPU " + str(index) + " : ADL_Overdrive5_CurrentActivity_Get failed.")
				else:
	
					fan_speed = {}
					for speed_type in (ADL_DL_FANCTRL_SPEED_TYPE_PERCENT, ADL_DL_FANCTRL_SPEED_TYPE_RPM):    
						fan_speed_value = ADLFanSpeedValue()
						fan_speed_value.iSize = sizeof(fan_speed_value)
						fan_speed_value.iSpeedType = speed_type
					
						if ADL_Overdrive5_FanSpeed_Get(info.iAdapterIndex, 0, byref(fan_speed_value)) != ADL_OK:
							fan_speed[speed_type] = None
							continue
					
						fan_speed[speed_type] = fan_speed_value.iFanSpeed
						user_defined = fan_speed_value.iFlags & ADL_DL_FANCTRL_FLAG_USER_DEFINED_SPEED
					
						
					if bool(fan_speed[ADL_DL_FANCTRL_SPEED_TYPE_PERCENT]) is True:
						self.setd('fan', fan_speed[ADL_DL_FANCTRL_SPEED_TYPE_PERCENT])
					else:
						self.setd('fan', -1)
	
					
					if bool(fan_speed[ADL_DL_FANCTRL_SPEED_TYPE_RPM]) is True:
						self.setd('fan_rpm', fan_speed[ADL_DL_FANCTRL_SPEED_TYPE_RPM])
					else:
						self.setd('fan_rpm', -1)
					
					if (self.getd('fan') > -1) and (self.getd('fan') % 5 == 0):	
						if (temp_hist[index]['min'][ self.getd('fan') ] == 0) or (self.getd('temp') < temp_hist[index]['min'][self.getd('fan')]):
							temp_hist[index]['min'][self.getd('fan')] = self.getd('temp')
						if (temp_hist[index]['max'][self.getd('fan')] == 0) or (self.getd('temp') > temp_hist[index]['max'][self.getd('fan')]):
							temp_hist[index]['max'][self.getd('fan')] = self.getd('temp')
							
						if (temp_hist[index]['smp'][self.getd('fan')] == 0):
							temp_hist[index]['avg'][self.getd('fan')] = self.getd('temp')
							temp_hist[index]['tot'][self.getd('fan')] = self.getd('temp')	
						elif (temp_hist[index]['smp'][self.getd('fan')] >= 86400 / self.pcontrol.getADL_Time()):   # keep only 24 hrs average
							# throw away an "hour" or so.. whatevr
							temp_hist[index]['smp'][self.getd('fan')] = 82800 / self.pcontrol.getADL_Time()  
							temp_hist[index]['tot'][self.getd('fan')] = temp_hist[index]['tot'][self.getd('fan')] / 24 * 23
						else:
							temp_hist[index]['tot'][self.getd('fan')] = temp_hist[index]['tot'][self.getd('fan')] + self.getd('temp')
							temp_hist[index]['avg'][self.getd('fan')] = temp_hist[index]['tot'][self.getd('fan')] / (temp_hist[index]['smp'][self.getd('fan')] + 1)
						
						temp_hist[index]['smp'][self.getd('fan')] = temp_hist[index]['smp'][self.getd('fan')] + 1
						temp_hist[index]['lst'][self.getd('fan')] = time.time() 

					# fan speed mgmt


					if (overheating > 0):
						# all fans to max if anyone overheats
						if (self.getd('fan') > -1) & (self.getd('fan') < self.pcontrol.getFanMaxSpeed(index)):
							self.evtlog.append(self.getts() + ": GPU " + str(index) + " : set fan to max (a GPU is overheated)")
							self.set_fan(info.iAdapterIndex, self.pcontrol.getFanMaxSpeed(index))
							del temps[index][:]	
					else:
						if (self.checkGPUOverMax(temps, index)):
							# over max average during period
							if (self.getd('fan') > -1) and (self.getd('fan') < self.pcontrol.getFanMaxSpeed(index)):
								# normal up, jump to 100% briefly to cool, but fallback to +5 once back in limits
								newfan = self.pcontrol.getFanMaxSpeed(index)
								fallback[index] = min(self.pcontrol.getFanMaxSpeed(index),self.getd('fan') + 5)
								self.evtlog.append(self.getts() + ": GPU " + str(index) + " : fan speed increased to " + str(newfan) + "%")
								self.set_fan( info.iAdapterIndex, newfan )
								del temps[index][:]
								
							elif (self.pcontrol.getAutoCoreLo(index) > -1):
								# we're overheating at max fan...  can we drop gpu clock?
								if ( (activity.iEngineClock/100.0) > self.pcontrol.getAutoCoreLo(index) ):
									newclock = max( self.pcontrol.getAutoCoreLo(index), (activity.iEngineClock/100.0) - self.pcontrol.getAutoCoreStep(index) )
									self.evtlog.append(self.getts() + ": GPU " + str(index) + " : Reduce core clock to " + str(newclock) + " due to overheat")
									self.set_plevels( [ index ], [ 0,1,2 ], newclock)
									del temps[index][:]
								
						else:
							newfan = self.getd('fan')
							
							if (fallback[index] > 0) and ( self.getd('temp') < self.pcontrol.getTargetTempMax(index) ):
								# fast cool/drop back to fallback rate
								newfan = fallback[index]
								self.evtlog.append(self.getts() + ": GPU " + str(index) + " : fan speed returned to " + str(newfan) + "%")
								self.set_fan(info.iAdapterIndex, newfan)
								fallback[index] = 0
								del temps[index][:]
								
							elif self.checkGPUUnderMin(temps, index):
								if (self.pcontrol.getAutoCoreHi(index) > (activity.iEngineClock/100.0)):
									# we are underclocked, try core bump at same fan
									newclock = min( self.pcontrol.getAutoCoreHi(index), (activity.iEngineClock/100.0) + self.pcontrol.getAutoCoreStep(index) )
									self.evtlog.append(self.getts() + ": GPU " + str(index) + " : Increase core clock to " + str(newclock) + " due to low temp")
									self.set_plevels( [ index ], [ 2 ], newclock)
									del temps[index][:]
									
								elif (self.getd('fan') > -1) and (self.getd('fan') > self.pcontrol.getFanMinSpeed(index)):
									# normal drop, lower tier has max < target
									newfan = max(self.getd('fan') - 5, self.pcontrol.getFanMinSpeed(index))
									self.evtlog.append(self.getts() + ": GPU " + str(index) + " : fan speed reduced to " + str(newfan) + "%")
									self.set_fan(info.iAdapterIndex, newfan)
									del temps[index][:]
								
				
				if (self.pcontrol.getInMenu() == 0):
					if (self.pcontrol.getDisplayType() == 0):
						
						spr = 3
						if (self.pcontrol.getNumGPUs() > 4):
							spr = 2
						
						self.scr.addstr(4 + index*spr, 1, str(index))
						self.scr.addstr(4 + index*spr,58, "%20s" % " ")
						self.scr.addstr(5 + index*spr,58, "%20s" % " ")
						
						td = 73
						
						tattr = self.getTattr(index, self.getd('temp'))
					
						self.scr.addstr(4 + index*spr, td, "%2.1fc" % self.getd('temp'), tattr)
							
						self.scr.addstr(4 + index*spr, 58, "%g%%" % (activity.iActivityPercent))
						self.scr.addstr(4 + index*spr, 63, "%g/%g" % ((activity.iEngineClock/100.0) , (activity.iMemoryClock/100.0) ))
						#self.scr.addstr(4 + index*2, 73, "%g" % (activity.iVddc/1000.0))
						self.scr.addstr(5 + index*spr, 58, "Fan: %4d rpm (%3d%%)" % (self.getd('fan_rpm'), self.getd('fan') ))
						
						
					elif (self.pcontrol.getDisplayType() == 3):
						
						top = 4
						
						if (int(time.time()) % 10 < 5):
							self.scr.addstr(top + 18, 2, "%-15s" % 'up/down to', curses.color_pair(8)  | curses.A_BOLD)
							self.scr.addstr(top + 19, 2, "%-15s" % 'switch GPU', curses.color_pair(8)  | curses.A_BOLD)
						else:
							self.scr.addstr(top + 18, 2, "%-15s" % 'enter to open', curses.color_pair(8)  | curses.A_BOLD)
							self.scr.addstr(top + 19, 2, "%-15s" % 'GPU settings', curses.color_pair(8)  | curses.A_BOLD)
						
						self.scr.addstr(4 + index*2, 1, "%15s" % " ")	
						self.scr.addstr(5 + index*2, 1, "%15s" % " ")	
												
						if (self.pcontrol.getCurrentGPU() == index):
							self.scr.addstr(4 + index*2, 1, '>' + str(index), curses.A_BOLD)
						else:
							self.scr.addstr(4 + index*2, 2, str(index), curses.color_pair(8)  | curses.A_BOLD )
							
						td = 4
						tattr = self.getTattr(index, self.getd('temp'))
						
						self.scr.addstr(4 + index*2, td, "%4.1fc" % self.getd('temp'), tattr)
						
						if (len(temps[index]) > 0): 
							ttmp = sum(temps[index][:self.pcontrol.getTempPeriodDown(index)]) / min(self.pcontrol.getTempPeriodDown(index), len(temps[index]) )
							tattr = self.getTattr(index, ttmp)
							if (self.pcontrol.getCurrentGPU() != index):
									tattr = tattr ^ curses.A_BOLD
							self.scr.addstr(5 + index*2, td, "%3.1fc" % ttmp, tattr)
							ttmp = sum(temps[index][:self.pcontrol.getTempPeriodUp(index)]) / min(self.pcontrol.getTempPeriodUp(index), len(temps[index]) )
							tattr = self.getTattr(index, ttmp)
							if (self.pcontrol.getCurrentGPU() != index):
									tattr = tattr ^ curses.A_BOLD
							self.scr.addstr(5 + index*2, td + 6, "%3.1fc" % ttmp, tattr)
									
						if (self.pcontrol.getCurrentGPU() == index):
							self.scr.addstr(4 + index*2, 10, "F:%2d%%" % self.getd('fan'), curses.A_BOLD)
							
								
							# fan data
							# self.scr.addstr(3,17,"Fan %  Min Temp  Avg Temp  Max Temp    Time     Last", curses.color_pair(8)  | curses.A_BOLD)
							
							rt = 23
							
							for x in range(0,21):
								
								if (x*5 == self.getd('fan')):
									attr = curses.A_BOLD
								elif (x*5 == fallback[index]) and (x > 0):
									attr = curses.color_pair(5)
								elif (temp_hist[index]['smp'][(x*5)] < 1):
									attr =  curses.color_pair(8)  | curses.A_BOLD
								else:
									attr = 0
								
								self.scr.addstr(top + x,rt, ("%" + str(79 - rt) + "s") % " ")
								
								if (x*5 == self.getd('fan')):
									self.scr.addstr(top + x,rt+1, ">%3d" % (x*5), attr)
								else:
									self.scr.addstr(top + x,rt+2, "%3d" % (x*5), attr)
									
								ttemp = temp_hist[index]['min'][(x*5)]
								
								tattr = self.getTattr(index, ttemp)
								
								if (x*5 != self.getd('fan')) and (ttemp > 0):
									tattr = tattr ^ curses.A_BOLD
								
								self.scr.addstr(top + x,rt+10, "%4.1f" % temp_hist[index]['min'][(x*5)] + 'c', tattr)
								
								ttemp = temp_hist[index]['avg'][(x*5)] 
								
								tattr = self.getTattr(index, ttemp)
								
								if (x*5 != self.getd('fan')) and (ttemp > 0):
									tattr = tattr ^ curses.A_BOLD
									
								self.scr.addstr(top + x,rt+19, "%5.2f" % ttemp, tattr)
								
								
								ttemp = temp_hist[index]['max'][(x*5)]
								
								tattr = self.getTattr(index, ttemp)
								
								if (x*5 != self.getd('fan')) and (ttemp > 0):
									tattr = tattr ^ curses.A_BOLD
								
								self.scr.addstr(top + x,rt+30, "%4.1f" % temp_hist[index]['max'][(x*5)] + 'c', tattr)
									
									
								self.scr.addstr(top + x,rt+39,  self.prettyTimeTotal(temp_hist[index]['smp'][(x*5)] * self.pcontrol.getADL_Time()), attr)
								self.scr.addstr(top + x,rt+48,  self.prettyTimeStamp(temp_hist[index]['lst'][(x*5)]), attr)
						else:
							self.scr.addstr(4 + index*2, 10, "F:%2d%%" % self.getd('fan'), curses.color_pair(8)  | curses.A_BOLD)			
							
							
					elif (self.pcontrol.getDisplayType() == 4) and (index == self.pcontrol.getCurrentGPU()):							
						# GPU details/settings
						
						
						self.scr.addstr(4, 1, " GPU " + str(index) + " Details  ")
						
						tattr = self.getTattr(index, self.getd('temp'))
						
						goff = 16
						
						for x in range(goff,23):
							self.scr.addstr(x, 1, "%24s" % " ")
						
						
						self.scr.addstr(goff,2, "Temp: %2.1fc" % self.getd('temp'), tattr)
						
						self.scr.addstr(goff+2, 3, "Fan: %3d rpm" % self.getd('fan_rpm'))
						
						self.scr.addstr(goff+3, 2, "Fan%%: %g%%" % self.getd('fan'))
						
						self.scr.addstr(goff+4, 2, "Load: %g%%" % (activity.iActivityPercent))
						self.scr.addstr(goff+5, 2, "Core: %g Mhz" % (activity.iEngineClock/100.0))
						self.scr.addstr(goff+6, 2, " Mem: %g Mhz" % (activity.iMemoryClock/100.0) )
						self.scr.addstr(goff+7, 2, " Pwr: %g vdc" % (activity.iVddc/1000.0))
						
						da = curses.color_pair(8)  | curses.A_BOLD
						sa = 0
						
						gy = 11
						gx = 35
						
						self.scr.addstr(gy, gx, " O/C:   Prof 0:   Prof 1:   Prof 2:")
						self.scr.addstr(gy+1, gx, "Core:")
						
						for x in range(0,3):
							if (self.pcontrol.hasGPUSetting(index, "core_speed_" + str(x))):
								self.scr.addstr(gy+1, gx + 7 + (x*10), "%4d Mhz" % self.pcontrol.getGPUSetting(index, "core_speed_" + str(x)), sa)
							else:
								self.scr.addstr(gy+1, gx + 7 + (x*10), " not set", da)
								
						self.scr.addstr(gy+2, gx, " Mem:" )
						
						for x in range(0,3):
							if (self.pcontrol.hasGPUSetting(index, "mem_speed_" + str(x))):
								self.scr.addstr(gy+2, gx + 7 + (x*10), "%4d Mhz" % self.pcontrol.getGPUSetting(index, "mem_speed_" + str(x)), sa)
							else:
								self.scr.addstr(gy+2, gx + 7 + (x*10), " not set", da)
						
						self.scr.addstr(gy+3, gx, " Pwr:")
						
						for x in range(0,3):
							if (self.pcontrol.hasGPUSetting(index, "core_voltage_" + str(x))):
								self.scr.addstr(gy+3, gx + 7 + (x*10), "%5.4dvdc" % self.pcontrol.getGPUSetting(index, "core_voltage_" + str(x)), sa)
							else:
								self.scr.addstr(gy+3, gx + 7 + (x*10), " not set", da)
						

						gy = 17
						gx = 30
						
						self.scr.addstr(gy, gx, "  AutoFan ",  curses.A_BOLD )
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_target_max") or self.pcontrol.hasGPUSetting(index, "autofan_target_min") ):
							ea = sa
						else:
							ea = da
						self.scr.addstr(gy+1, gx, "   target:       - ", ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_target_min")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getTargetTempMin(index)
						self.scr.addstr(gy+1, gx+11, "%3d c" % d, ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_target_max")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getTargetTempMax(index)
						self.scr.addstr(gy+1, gx+18, "%3d c" % d, ea)
						
					
					
					
						if (self.pcontrol.hasGPUSetting(index, "autofan_max") or self.pcontrol.hasGPUSetting(index, "autofan_min") ):
							ea = sa
						else:
							ea = da
						self.scr.addstr(gy+2, gx, "    speed:       - ", ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_min")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getFanMinSpeed(index)
						self.scr.addstr(gy+2, gx+12, "%-3d%%" % d, ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_max")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getFanMaxSpeed(index)
						self.scr.addstr(gy+2, gx+19, "%-3d%%" % d, ea)
					
					
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_overheat")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getOverheatTemp(index)
						
						self.scr.addstr(gy+3, gx, " overheat: %3d c" % d, ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_cutoff")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getCutoffTemp(index)
						
						self.scr.addstr(gy+4, gx, "   cutoff: %3d c" % d, ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_period_up")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getTempPeriodUp(index)
						
						self.scr.addstr(gy+5, gx, "period_up: %3d sec" % d, ea)
						
						
						if (self.pcontrol.hasGPUSetting(index, "autofan_period_down")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getTempPeriodDown(index)
						
						self.scr.addstr(gy+6, gx, "period_dn: %3d sec" % d, ea)
						
						gy = 17
						gx = 56
						
						self.scr.addstr(gy, gx, " AutoCore ",  curses.A_BOLD )
						
						if (self.pcontrol.hasGPUSetting(index, "autocore_speed_lo")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getAutoCoreLo(index)
						
						self.scr.addstr(gy+1, gx, " speed_lo: %3d Mhz" % d, ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autocore_speed_hi")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getAutoCoreHi(index)
						
						self.scr.addstr(gy+2, gx, " speed_hi: %3d Mhz" % d, ea)
						
						if (self.pcontrol.hasGPUSetting(index, "autocore_speed_step")):
							ea = sa
						else:
							ea = da
						d = self.pcontrol.getAutoCoreStep(index)
						
						self.scr.addstr(gy+3, gx, "     step: %3d Mhz" % d, ea)
						
					
						
					
			(y,x) = self.scr.getmaxyx()
				
			while (len(self.evtlog) > 1000):
				self.evtlog.pop(0)
			
			if (x > 78) and (self.pcontrol.getInMenu() == 0):
				
				# header
				self.scr.addstr(0,5, ("%" + str(74 - len(self.pcontrol.getSetting('miner_id'))) + "s") % " ") 
				
				tmpt = 0
				tmpa = 0
				tmpr = 0
				
				for i in range(0, self.pcontrol.getNumGPUs()):
					if 'hash' in self.pcontrol.getGPUData(i).keys():
						tmpt = tmpt + float(( self.pcontrol.getGPUData(i))['hash'])
					if 'accept' in self.pcontrol.getGPUData(i).keys():
						tmpa = tmpa + int(( self.pcontrol.getGPUData(i))['accept'])
					if 'reject' in self.pcontrol.getGPUData(i).keys():
						tmpr = tmpr + int(( self.pcontrol.getGPUData(i))['reject'])
						
				tmps = "%6.2f M/h" % tmpt
				
				tmps = tmps + "  %d/%d" % ( tmpa, tmpr )
				if (tmpa + tmpr) > 0:
					 tmps = tmps +  "  %4.2f%%" % ( float(tmpr / float(tmpr + tmpa)) * 100.0  )
					 
				self.scr.addstr(0, x/2 - int(len(tmps) / 2), tmps )
				
				# log
			
				ltop = 26
			
				if (self.pcontrol.getDisplayType() == 0):
					if (self.pcontrol.getNumGPUs() > 4):
						ltop = 5 + self.pcontrol.getNumGPUs()*2
					else:
						ltop = 5 + self.pcontrol.getNumGPUs()*3
				elif (self.pcontrol.getDisplayType() == 2):
					ltop = 4
			
				for i in range(ltop, y):
					if (len(self.evtlog) > (i-ltop)):
						self.scr.addstr(i,1,"%-78s" % self.evtlog[(i-ltop+1)*-1])

			self.scr.refresh()
			
			sleepstart = time.time()
			
			while (time.time() - sleepstart < self.pcontrol.getADL_Time() ) and (self.pcontrol.getDrawNow() == False):
				time.sleep(0.1)

	def prettyTimeTotal(self, tottime):
		res = ""
		
		if (tottime == 0):
			res = "None"
		elif (tottime > 86400):
			res = str(int(tottime / 86400)) + "d " +  "%02d" % int((tottime % 86400) / 3600) + "h"
		elif (tottime > 3600):
			res = "%02d" % int(tottime / 3600) + "h " + "%02d" % int((tottime % 3600) / 60) + "m"
		else:
			res = "%02d" % int(tottime / 60) + "m " + "%02d" % int(tottime % 60) + "s"
		return(res)
		
	def prettyTimeStamp(self, curtime):
		res = str(curtime)
		
		if (curtime == 0):
			res = "Never"
		elif (time.time() - curtime < 86400):
			dt = datetime.datetime.fromtimestamp(int(curtime))
			res = dt.strftime("%H:%M:%S")
		else:
			dt = datetime.datetime.fromtimestamp(int(curtime))
			res = dt.strftime("%m/%d %H:%M")
			
		return(res)
	

	def initialize(self):
		# check for unset DISPLAY, assume :0
		if "DISPLAY" not in os.environ:
			os.environ["DISPLAY"] = ":0"
		
		# the '1' means only retrieve info for active adapters
		if ADL_Main_Control_Create(ADL_Main_Memory_Alloc, 1) != ADL_OK:
			raise ADLError("Couldn't initialize ADL interface.")
	
	def shutdown():
		if ADL_Main_Control_Destroy() != ADL_OK:
			raise ADLError("Couldn't destroy ADL interface global pointers.")
      
			
	def get_adapter_info(self):
		adapter_info = []
		num_adapters = c_int(-1)
		if ADL_Adapter_NumberOfAdapters_Get(byref(num_adapters)) != ADL_OK:
			raise ADLError("ADL_Adapter_NumberOfAdapters_Get failed.")
	
		# allocate an array of AdapterInfo, see ctypes docs for more info
		AdapterInfoArray = (AdapterInfo * num_adapters.value)() 
		
		# AdapterInfo_Get grabs info for ALL adapters in the system
		if ADL_Adapter_AdapterInfo_Get(cast(AdapterInfoArray, LPAdapterInfo), sizeof(AdapterInfoArray)) != ADL_OK:
			raise ADLError("ADL_Adapter_AdapterInfo_Get failed.")
	
		deviceAdapter = collections.namedtuple('DeviceAdapter', ['AdapterIndex', 'AdapterID', 'BusNumber', 'UDID'])
		devices = []
		
		for adapter in AdapterInfoArray:
			index = adapter.iAdapterIndex
			busNum = adapter.iBusNumber
			udid = adapter.strUDID
			
			adapterID = c_int(-1)
			#status = c_int(-1)
			
			if ADL_Adapter_ID_Get(index, byref(adapterID)) != ADL_OK:
				raise ADLError("ADL_Adapter_Active_Get failed.")
			
			found = False
			for device in devices:
				if (device.AdapterID.value == adapterID.value):
					found = True
					break
			
			# save it in our list if it's the first controller of the adapter
			if (found == False):
				devices.append(deviceAdapter(index,adapterID,busNum,udid))
		
		for device in devices:
			adapter_info.append(AdapterInfoArray[device.AdapterIndex])
		
		return adapter_info
	



	
